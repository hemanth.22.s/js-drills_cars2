const problem4 = (inventory) => {
    let yearsCar = [];
    inventory.map(car => yearsCar.push(car.car_year));
    return yearsCar;
}

module.exports = problem4;