const problem3 = (inventory) => {
    let models = [];
    inventory.map(car => models.push(car.car_model));
    let sortModels = models.sort();
    return sortModels;  
}

module.exports = problem3;

