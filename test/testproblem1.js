const problem1 = require('../problem1')

const inventory = require('./inventory')

let searchId = 33;

const result = problem1(inventory,searchId)
console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`)
